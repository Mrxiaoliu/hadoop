package com.lagou;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HbaseClientDemo {
    Configuration conf = null;
    Connection conn = null;
    HBaseAdmin admin = null;

    @Before
    public void init() throws IOException {
        conf = HBaseConfiguration.create();
        conf.set("hbase.zookeeper.quorum", "newhadoop1,newhadoop2,newhadoop3");
        conf.set("hbase.zookeeper.property.clientPort", "2181");
        conn = ConnectionFactory.createConnection(conf);
    }

    public void destroy() {
        if (admin != null) {
            try {
                admin.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void createTable() throws IOException {
        admin = (HBaseAdmin) conn.getAdmin();
        //创建表描述器器
        HTableDescriptor friends = new HTableDescriptor(TableName.valueOf("friends"));
        //设置列列族描述器器
        friends.addFamily(new HColumnDescriptor("friends"));
        //执⾏行行创建操作
        admin.createTable(friends);
        System.out.println("friends表创建成功！！ ");
    }

    //插⼊入数据
    @Test
    public void putData() throws IOException {
        //获取⼀一个表对象
        Table t = conn.getTable(TableName.valueOf("friends"));
        List<Put> list = new ArrayList<Put>();
        //设定rowkey
        Put put = new Put(Bytes.toBytes("1"));
        //列列族，列列， value
        put.addColumn(Bytes.toBytes("friends"), Bytes.toBytes("2"), Bytes.toBytes("2"));
        put.addColumn(Bytes.toBytes("friends"), Bytes.toBytes("3"), Bytes.toBytes("3"));
        list.add(put);
        //设定rowkey
        Put put1 = new Put(Bytes.toBytes("2"));
        //列列族，列列， value
        put1.addColumn(Bytes.toBytes("friends"), Bytes.toBytes("1"), Bytes.toBytes("1"));
        put1.addColumn(Bytes.toBytes("friends"), Bytes.toBytes("3"), Bytes.toBytes("3"));
        list.add(put1);
        Put put2 = new Put(Bytes.toBytes("3"));
        //列列族，列列， value
        put2.addColumn(Bytes.toBytes("friends"), Bytes.toBytes("1"), Bytes.toBytes("1"));
        put2.addColumn(Bytes.toBytes("friends"), Bytes.toBytes("2"), Bytes.toBytes("2"));
        list.add(put2);
        //执⾏行行插⼊入
        t.put(list);
        // t.put();//可以传⼊入list批量量插⼊入数据删除数据：
        //关闭table对象
        t.close();
        System.out.println("插⼊入成功！！ ");
    }

    /**
     * 全表扫描
     */
    @Test
    public void scanAllData() throws IOException {
        HTable friends = (HTable) conn.getTable(TableName.valueOf("friends"));
        Scan scan = new Scan();
        ResultScanner resultScanner = friends.getScanner(scan);
        for (Result result : resultScanner) {
            Cell[] cells = result.rawCells();//获取改⾏行行的所有cell对象
            for (Cell cell : cells) {
                //通过cell获取rowkey,cf,column,value
                String cf = Bytes.toString(CellUtil.cloneFamily(cell));
                String column = Bytes.toString(CellUtil.cloneQualifier(cell));
                String value = Bytes.toString(CellUtil.cloneValue(cell));
                String rowkey = Bytes.toString(CellUtil.cloneRow(cell));
                System.out.println(rowkey + "----" + cf + "--" + column + "---" + value);
            }
        }
        friends.close();
    }


    @Test
    public void deleteFriend() throws IOException {
        HTable friends = (HTable) conn.getTable(TableName.valueOf("friends"));
        String ownRowkey = "1";
        String friendRowkey = "2";
        //创建查询的get对象
        Get get = new Get(Bytes.toBytes(ownRowkey));
        //指定列列族信息
        get.addColumn(Bytes.toBytes("friends"), Bytes.toBytes(friendRowkey));
        get.addFamily(Bytes.toBytes("friends"));
        //执⾏行行查询
        Result res = friends.get(get);
        Cell[] cells = res.rawCells();//获取改⾏行行的所有cell对象
        List<Delete> list = new ArrayList<Delete>();
        for (Cell cell : cells) {
            //通过cell获取rowkey,cf,column,value
            String cf = Bytes.toString(CellUtil.cloneFamily(cell));
            String column = Bytes.toString(CellUtil.cloneQualifier(cell));
            String value = Bytes.toString(CellUtil.cloneValue(cell));
            String rowkey = Bytes.toString(CellUtil.cloneRow(cell));
            if (column.equals(friendRowkey)) {
                //先删除自己的
                Delete de = new Delete(Bytes.toBytes(ownRowkey));
                de.deleteColumn(Bytes.toBytes("friends"), Bytes.toBytes(value));
                list.add(de);
                //删除别人的
                Get get1 = new Get(Bytes.toBytes(friendRowkey));
                get.addColumn(Bytes.toBytes("friends"), Bytes.toBytes(ownRowkey));
                get.addFamily(Bytes.toBytes("friends"));
                Result res1 = friends.get(get1);
                Cell[] cells1 = res1.rawCells();//获取改⾏行行的所有cell对象
                for (Cell cell1 : cells1) {
                    //通过cell获取rowkey,cf,column,value
                    String cf1 = Bytes.toString(CellUtil.cloneFamily(cell1));
                    String column1 = Bytes.toString(CellUtil.cloneQualifier(cell1));
                    String value1 = Bytes.toString(CellUtil.cloneValue(cell1));
                    String rowkey1 = Bytes.toString(CellUtil.cloneRow(cell1));
                    if (column1.equals(ownRowkey)) {
                        Delete de1 = new Delete(Bytes.toBytes(friendRowkey));
                        de1.deleteColumn(Bytes.toBytes("friends"), Bytes.toBytes(value1));
                        list.add(de1);
                    }
                }
            }
        }
        friends.delete(list);
        friends.close();//关闭表对象资源
    }

    //删除⼀一条数据
    @Test
    public void deleteData() throws IOException {
        //需要获取⼀一个table对象
        Table friends = conn.getTable(TableName.valueOf("friends"));
        //准备delete对象
        Delete delete = new Delete(Bytes.toBytes("1"));
        delete.deleteColumn(Bytes.toBytes("friends"), Bytes.toBytes("friend1"));
         //执⾏行行删除
        friends.delete(delete);
        //关闭table对象
        friends.close();
        System.out.println("删除数据成功！！ ");
    }
}