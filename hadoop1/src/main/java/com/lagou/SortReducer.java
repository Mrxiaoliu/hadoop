package com.lagou;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class SortReducer extends Reducer <IntWritable, IntWritable, IntWritable,IntWritable> {
    private IntWritable postion = new IntWritable(1); //存放名次
    @Override
    protected void reduce(IntWritable key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        for (IntWritable item :values){ //同一个数字可能出多次，就要多次并列排序
            context.write(postion,key); //写入名次和具体数字
            System.out.println(postion + "\t"+ key);
            postion = new IntWritable(postion.get()+1); //名次加1
        }
    }
}
