package com.lagou;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class SortMapper extends Mapper <LongWritable, Text,IntWritable,IntWritable>{
    private IntWritable mapperValue = new IntWritable();
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString();
        mapperValue.set(Integer.parseInt(line));
        context.write(mapperValue,new IntWritable(1));
    }
}
