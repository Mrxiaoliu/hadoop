package com.lagou;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class HdfsClient {
    @Test
    public void testMkdirs() throws IOException, InterruptedException,
            URISyntaxException {
        // 1 获取文件系统
        Configuration configuration = new Configuration();
        // 配置在集群上运行
        // configuration.set("fs.defaultFS", "hdfs://linux121:9000");
        // FileSystem fs = FileSystem.get(configuration);
        FileSystem fs = FileSystem.get(new URI("hdfs://192.168.1.131:9000"),
                configuration, "root");
        // 2 创建目录
        fs.mkdirs(new Path("/test"));
        // 3 关闭资源
        fs.close();
    }

    @Test
    public void testCopyToLocalFile() throws IOException, InterruptedException,
            URISyntaxException {
        Configuration configuration = new Configuration();
        FileSystem fs = FileSystem.get(new URI("hdfs://192.168.1.131:9000"),
                configuration, "root");
        fs.copyFromLocalFile(new Path("C:\\Users\\Administrator\\Desktop\\hadoop\\file\\file1.txt"), new Path("/test"));
        fs.copyFromLocalFile(new Path("C:\\Users\\Administrator\\Desktop\\hadoop\\file\\file2.txt"), new Path("/test"));
        fs.copyFromLocalFile(new Path("C:\\Users\\Administrator\\Desktop\\hadoop\\file\\file3.txt"), new Path("/test"));
        fs.close();
    }

    @Test
    public void testDelete() throws IOException, InterruptedException,
            URISyntaxException {
        // 1 获取文件系统
        Configuration configuration = new Configuration();
        FileSystem fs = FileSystem.get(new URI("hdfs://192.168.1.131:9000"),
                configuration, "root");
        // 2 执行删除
        fs.delete(new Path("/test"), true);
        // 3 关闭资源
        fs.close();
    }
}